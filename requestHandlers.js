var querystring = require('querystring')
	, fs = require('fs')
	, formidable = require('formidable');

function start(response,postData) {
	console.log('Request handler "start" was called!!! ');

	var body = '<html>'+ 
	'<head>'+
	'<meta http-equiv="Content-Type" content="text/html; '+
	'charset=UTF-8" />'+
	'</head>'+
	'<body>'+
	'<form action="/upload" enctype="multipart/form-data" method="post">'+
	'<input name="file" name="upload">'+
	'<input type="submit" value="Upload File" />'+
	'</form>'+
	'</body>'+
	'</html>'

	response.writeHead(200,{'Content-Type': 'text/html'});
	response.write(body);
	response.end();
	
};

function upload(response,request) {
	console.log('Request handler "upload" was called!!! ');
	
	var form = new formidable.IncomingForm();
	console.log('About to Parse');
	form.parse(request, function(error, fields, files) {
		console.log('parsing complete');

		// Possible Error On Windows if tried to rename an existing file!!
		fs.rename(files.upload.path, "/temp/test.png", function(err) {
			if (err) {
				fs.fs.unlink("/tmp/test.png");
			};
		});
	});

	response.writeHead(200,{'Content-Type': 'text/plain'});
	response.write("You've sent" + querystring.parse().text);
	response.end()

}

function show (response) {
	console.log('Request Handler "show" was called. ');
	response.writeHead(200, {"Content-Type":"image/png"});
	fs.createReadStream("/tmp/test.png").pipe(response);
};

exports.start = start;
exports.upload = upload;
exports.show = show;

